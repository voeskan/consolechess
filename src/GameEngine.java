import static java.lang.Character.isLowerCase;

public class GameEngine {
    private char[][] board;
    private boolean isTurnOfPlayerSmall;

    public GameEngine() {
        board = initBoard();
        isTurnOfPlayerSmall = true;
    }

    public boolean processTurn(String currentInput) {
        //TODO
        return true;
    }

    public char[][] getBoard() {
        return board;
    }

    public boolean isTurnOfPlayerSmall() {
        return isTurnOfPlayerSmall;
    }


    private boolean isEmptyField(char field) {
        return field == ' ';
    }


    private boolean hasPieceOfPlayerSmall(char field) {
        return isLowerCase(field);
    }

    private char[][] initBoard() {
        board = new char[8][8];
        board[7][0] = 'R'; board[7][1] = 'K'; board[7][2] = 'B'; board[7][3] = 'Q'; board[7][4] = 'X'; board[7][5] = 'B'; board[7][6] = 'K'; board[7][7] = 'R';
        board[6][0] = 'P'; board[6][1] = 'P'; board[6][2] = 'P'; board[6][3] = 'P'; board[6][4] = 'P'; board[6][5] = 'P'; board[6][6] = 'P'; board[6][7] = 'P';
        board[5][0] = ' '; board[5][1] = ' '; board[5][2] = ' '; board[5][3] = ' '; board[5][4] = ' '; board[5][5] = ' '; board[5][6] = ' '; board[5][7] = ' ';
        board[4][0] = ' '; board[4][1] = ' '; board[4][2] = ' '; board[4][3] = ' '; board[4][4] = ' '; board[4][5] = ' '; board[4][6] = ' '; board[4][7] = ' ';
        board[3][0] = ' '; board[3][1] = ' '; board[3][2] = ' '; board[3][3] = ' '; board[3][4] = ' '; board[3][5] = ' '; board[3][6] = ' '; board[3][7] = ' ';
        board[2][0] = ' '; board[2][1] = ' '; board[2][2] = ' '; board[2][3] = ' '; board[2][4] = ' '; board[2][5] = ' '; board[2][6] = ' '; board[2][7] = ' ';
        board[1][0] = 'p'; board[1][1] = 'p'; board[1][2] = 'p'; board[1][3] = 'p'; board[1][4] = 'p'; board[1][5] = 'p'; board[1][6] = 'p'; board[1][7] = 'p';
        board[0][0] = 'r'; board[0][1] = 'k'; board[0][2] = 'b'; board[0][3] = 'q'; board[0][4] = 'x'; board[0][5] = 'b'; board[0][6] = 'k'; board[0][7] = 'r';
        return board;
    }
}
