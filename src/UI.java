import static java.lang.System.out;

public class UI {
    public void drawBoard(char[][] b) {
        out.println("        a     b     c     d     e     f     g    h    ");
        out.println("    -------------------------------------------------");
        out.println(" 8  |  " + b[7][0] + "  |  " + b[7][1] + "  |  " + b[7][2] + "  |  " + b[7][3] + "  |  " + b[7][4] + "  |  " + b[7][5] + "  |  " + b[7][6] + "  |  " + b[7][7] + "  |");
        out.println("    -------------------------------------------------");
        out.println(" 7  |  " + b[6][0] + "  |  " + b[6][1] + "  |  " + b[6][2] + "  |  " + b[6][3] + "  |  " + b[6][4] + "  |  " + b[6][5] + "  |  " + b[6][6] + "  |  " + b[6][7] + "  |");
        out.println("    -------------------------------------------------");
        out.println(" 6  |  " + b[5][0] + "  |  " + b[5][1] + "  |  " + b[5][2] + "  |  " + b[5][3] + "  |  " + b[5][4] + "  |  " + b[5][5] + "  |  " + b[5][6] + "  |  " + b[5][7] + "  |");
        out.println("    -------------------------------------------------");
        out.println(" 5  |  " + b[4][0] + "  |  " + b[4][1] + "  |  " + b[4][2] + "  |  " + b[4][3] + "  |  " + b[4][4] + "  |  " + b[4][5] + "  |  " + b[4][6] + "  |  " + b[4][7] + "  |");
        out.println("    -------------------------------------------------");
        out.println(" 4  |  " + b[3][0] + "  |  " + b[3][1] + "  |  " + b[3][2] + "  |  " + b[3][3] + "  |  " + b[3][4] + "  |  " + b[3][5] + "  |  " + b[3][6] + "  |  " + b[3][7] + "  |");
        out.println("    -------------------------------------------------");
        out.println(" 3  |  " + b[2][0] + "  |  " + b[2][1] + "  |  " + b[2][2] + "  |  " + b[2][3] + "  |  " + b[2][4] + "  |  " + b[2][5] + "  |  " + b[2][6] + "  |  " + b[2][7] + "  |");
        out.println("    -------------------------------------------------");
        out.println(" 2  |  " + b[1][0] + "  |  " + b[1][1] + "  |  " + b[1][2] + "  |  " + b[1][3] + "  |  " + b[1][4] + "  |  " + b[1][5] + "  |  " + b[1][6] + "  |  " + b[1][7] + "  |");
        out.println("    -------------------------------------------------");
        out.println(" 1  |  " + b[0][0] + "  |  " + b[0][1] + "  |  " + b[0][2] + "  |  " + b[0][3] + "  |  " + b[0][4] + "  |  " + b[0][5] + "  |  " + b[0][6] + "  |  " + b[0][7] + "  |");
        out.println("    -------------------------------------------------");
    }

    public void startingInfos() {
        out.println(" ### WILLKOMMEN!");
        out.println(" ### Züge im Format [Startposition][Zielposition] eingeben, zum Beispiel 'a2a3' für einen Bauernzug.'");
        out.println(" ### Gib 'exit' ein um das Spiel zu verlassen.");
    }

    public void requestInput(boolean isPlayerSmall) {
        if (isPlayerSmall) {
            out.print("Spieler Klein");
        } else {
            out.print("Spieler Groß");
        }
        out.print(", gib einen Zug ein: ");
    }

    public void invalidInput() {
        out.print("Ungültiger Zug, bitte wiederholen: ");
    }
}
