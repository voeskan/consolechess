import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static java.lang.Character.isLowerCase;
import static java.lang.System.in;

public class Chess {
    public static void main(String[] args) throws IOException {
        GameEngine engine = new GameEngine();
        UI ui = new UI();
        String currentInput;
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

        ui.startingInfos();

        while(true) {
            ui.drawBoard(engine.getBoard());
            ui.requestInput(engine.isTurnOfPlayerSmall());
            currentInput = reader.readLine();

            if(currentInput.equals("exit")) {
                break;
            }

            if(!engine.processTurn(currentInput)) {
                ui.invalidInput();
            }
        }
    }
}
